/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};

/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {

/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;

/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};

/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);

/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;

/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}


/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;

/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;

/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";

/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports, __webpack_require__) {

	"use strict";

	// Librerias
	var //formValidate     = require('./libs/validateFormLogin.js'),
	    //validateFormRegister  = require('./libs/validateFormRegister.js'),
	    Autofocus             = __webpack_require__(1),
	    Ubigeo                = __webpack_require__(2),
	    Navegador             = __webpack_require__(3),
	    twDialog              = __webpack_require__(4),
	    fbDialog              = __webpack_require__(5);

	// Componentes
	//var Posts = require('./components/post.js');

	//Modules
	var Home = __webpack_require__(6);
	var support = __webpack_require__(8);
	var Validate = __webpack_require__(9);
	var Register = __webpack_require__(11);


	var APP, UTIL;

	APP = {

	    /* Funciones y métodos que se aplican al site en general */
	    common: {
	        init: function() {

	            var browser = new Navegador();

	            if ( browser.isVersion('tablet') ) {
	                console.log("Es Android Tablet");
	            }

	        }
	    },


	    /* Home */
	    home: {
	        init: function() {
	            support.handlers();
	            var home = new Home();
	        }
	    },


	    /* Validate document */
	    validate: {
	        init: function() {
	            support.handlers();
	            var validate = new Validate();

	        }
	    },

	    /* Register */
	    register: {
	        init: function() {
	            support.handlers();
	            var register = new Register();
	        }
	    },

	    /* Confirmation */
	    confirmation: {
	        init: function() {
	            support.handlers();
	        }
	    },


	    /**
	     * @param  {String} anchor  Id de la capa a scrollear
	     * @return {...}
	     */
	    scrollToAnchor: function ( anchor ) {
	        var ancla = $(anchor);

	        $("html, body").animate({
	            scrollTop: ancla.offset().top - 75
	        }, 700);
	    }
	};


	/* Ejecutador de metodos según vista */
	UTIL = {
	    exec: function(controller, action) {
	        var ns = APP;

	        action = (action === undefined ? "init" : action);

	        if (controller !== "" && ns[controller] && typeof ns[controller][action] === "function") {
	            ns[controller][action]();
	        }
	    },

	    init: function() {
	        var body       = document.getElementById("identify"),
	            controller = body.getAttribute("data-controller"),
	            action     = body.getAttribute("data-action");

	        UTIL.exec("common");
	        UTIL.exec(controller);
	        UTIL.exec(controller, action);
	    }
	};

	$(document).on("ready", UTIL.init);


/***/ }),
/* 1 */
/***/ (function(module, exports) {

	/**
	 * AutoFocus module cambia el foco (focus) de un campo a otro, despues de
	 * teclear en un campo. El modulo esta hecho especialmente para el login de
	 * submiller
	 * @modulo libs/autofocus
	 *
	 * @param {String} id_inputDay     Id del campo día
	 * @param {String} id_inputMonth   Id del campo mes
	 * @param {String} id_inputYear    Id del campo año
	 * @param {String} id_inputDni     Id del campo DNI
	 * @constructor
	 */
	var AutoFocus = function ( id_inputDay, id_inputMonth, id_inputYear, id_inputDni ) {
	    "use strict";

	    var self = this;

	    var campoDia  = document.getElementById( id_inputDay ),
	        campoMes  = document.getElementById( id_inputMonth ),
	        campoAnho = document.getElementById( id_inputYear ),
	        campoDNI  = document.getElementById( id_inputDni );


	    this.saltoInput = function ( campo ) {

	        var campoActual = campo.id,
	            numDigitos  = campo.value.length,
	            charCode    = (campo.which) ? campo.which : campo.keyCode,
	            nextInput;

	        if ( charCode > 31 && (charCode < 48 || charCode > 57) ) {

	            return false;

	        } else {

	            if ( campoActual !== id_inputYear ) {

	                if ( numDigitos == 2 && campoActual !== id_inputDni ) {

	                    if (campoActual == id_inputDay) nextInput = id_inputMonth;
	                    else if (campoActual == id_inputMonth) nextInput = id_inputYear;
	                    else if (campoActual == id_inputYear) nextInput = id_inputDni;

	                    setTimeout(function () {
	                        document.getElementById(nextInput).focus();
	                    }, 10);
	                }

	            } else {

	                if ( numDigitos == 4 && campoActual !== id_inputDni ) {

	                    if (campoActual == id_inputDay) nextInput = id_inputMonth;
	                    else if (campoActual == id_inputMonth) nextInput = id_inputYear;
	                    else if (campoActual == id_inputYear) nextInput = id_inputDni;

	                    setTimeout(function () {
	                        document.getElementById(nextInput).focus();
	                    }, 10);
	                }
	            }
	        }
	    };

	    campoDNI.onkeyup = function () {
	        self.saltoInput(this);
	    };

	    campoDia.onkeyup = function () {
	        self.saltoInput(this);
	    };

	    campoMes.onkeyup = function () {
	        self.saltoInput(this);
	    };

	    campoAnho.onkeyup = function () {
	        self.saltoInput(this);
	    };

	};

	module.exports = AutoFocus;

/***/ }),
/* 2 */
/***/ (function(module, exports) {

	/**
	 * Ubigeo module: para rellenar los combos con los datos de ubigeo
	 * @modulo libs/ubigeo
	 */
	var Ubigeo = function() {

	    var self = this;

	    /**
	     *
	     * @param data
	     * @param id
	     * @param title
	     * @returns {void|*|jQuery}
	     */
	    this.fillOptions = function ( data, id, title ) {

	        var arrayHTML, i, item, len;

	        arrayHTML = [];

	        $(id).empty();
	        $(id).append('<option value="">' + title + '</option>');

	        i   = 0;
	        len = data.length;

	        while (i < len) {
	            item = '<option value="' + data[i]['id'] + '">' + data[i]['name'] + '</option>';
	            arrayHTML.push(item);
	            i++;
	        }

	        return $(id).append(arrayHTML);
	    };


	    /**
	     *
	     * @param code
	     * @param id
	     * @param title
	     */
	    this.callToAjax = function ( code, id, title ) {

	        var ubigeo = $("#ubigeo_url").val();

	        $.ajax({
	            type: 'GET',
	            url: ubigeo + '?parent='+code,
	            success: function(data) {

	                self.fillOptions( data.ubigeos, id, title );
	            },
	            dataType: 'JSON'
	        });
	    };
	};

	module.exports = Ubigeo;

/***/ }),
/* 3 */
/***/ (function(module, exports) {

	/**
	 * Validador de navegador y versión
	 * @modulo libs/browser
	 */
	var Browser = function() {

	    var os = {},
	        userAgent = navigator.userAgent;

	    os.webkit  = (userAgent.match(/WebKit\/([\d.]+)/) ? true : false);
	    os.android = (userAgent.match(/(Android)\s+([\d.]+)/) ? true : false);
	    os.mobile  = (os.android && userAgent.match(/Mobile/i) ? true : false);
	    os.tablet  = (os.android && !userAgent.match(/Mobile/i) ? true : false);
	    os.ipad    = (userAgent.match(/(iPad).*OS\s([\d_]+)/) ? true : false);
	    os.iphone  = (!os.ipad && userAgent.match(/(iPhone\sOS)\s([\d_]+)/) ? true : false);
	    os.ios8    = ((os.ipad || os.iphone) && userAgent.match(/8_/) ? true : false);
	    os.ios     = os.ipad || os.iphone;
	    os.wp      = (userAgent.match(/Windows Phone/i) ? true : false);
	    os.chrome  = (userAgent.match(/Chrome/) ? true : false);
	    os.opera   = (userAgent.match(/Opera/) ? true : false);
	    os.ie11    = (userAgent.match(/MSIE 11.0/i) || userAgent.match(/Trident\/7/i) ? true : false);
	    os.ie10    = (userAgent.match(/MSIE 10.0/i) || userAgent.match(/Trident\/6/i) ? true : false);
	    os.ie9     = (userAgent.match(/MSIE 9.0/i) || userAgent.match(/Trident\/5/i) ? true : false);
	    os.ie8     = (userAgent.match(/MSIE 8.0/i) || userAgent.match(/Trident\/4/i) ? true : false);
	    os.ie      = os.ie9 || os.ie10 || os.ie11;

	    /**
	     * Verifica el navegador y su versión según el parámetro browser
	     *
	     * @method  isVersion
	     * @param   {String}   browser   Tipo de navegador
	     * @returns {Boolean}
	     */
	    this.isVersion = function ( browser ) {
	        return os[browser];
	    }
	};

	module.exports = Browser;

/***/ }),
/* 4 */
/***/ (function(module, exports) {

	/**
	 * Modulo para compartir en Twitter
	 *
	 * @param  {String} message  Mensaje de menos de 140 carácteres
	 * @param  {String} link     URL a compartir
	 * @param  {String} hashtag  Hashtag o lista de hashtags, ejem:"peru,color,etc"
	 */
	var twShare = function ( message, link, hashtag ) {

	    var uri;

	    uri = 'https://twitter.com/intent/tweet?url=' + link + '&text=' +
	          encodeURI(message) + ' &hashtags=' + hashtag + '&display=popup';


	    window.open(
	        uri,
	        "",
	        "status = 1, height = 450, width = 620, resizable = 0"
	    );
	}

	module.exports = twShare;

/***/ }),
/* 5 */
/***/ (function(module, exports) {

	/**
	 * Modulo que contiene los métodos para compartir en FB:
	 * fbDialog.share_url
	 * fbDialog.share_dialog
	 *
	 * Requerimientos: Debes configurar una Facebook App
	 *                 https://developers.facebook.com/docs/javascript/quickstart/v2.5
	 */
	var fbDialog = {

	    /**
	     * FB Share Dialog: Puedes compartir un sitio en FB con una simple URL.
	     * Incluir info en la metadata de la página (URL) para personalizar el
	     * contenido compartido en Facebook.
	     * Más info: https://developers.facebook.com/docs/sharing/reference/share-dialog
	     *
	     * @param  {String}   url Link a compartir
	     * @return {Function}     Función que devuelve el estado de la operación
	     */
	    share_url: function ( url ) {

	        FB.ui({
	            method: 'share',
	            href: url,
	        }, function ( response ) {

	        });
	    },

	    /**
	     * [share_simple description]
	     * @param  {String} url URL a compartir
	     * @return PopUp de facebook para compartir
	     */
	    share: function ( url ) {
	        var href = "http://www.facebook.com/share.php?u=" + url;

	        window.open(
	            href,
	            '',
	            'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600'
	        );
	    },



	    /**
	     * FB Feed Dialog: Añade un diálogo en la aplicación para que la gente
	     * pueda publicar historias individuales a su línea de tiempo.
	     * Más info: https://developers.facebook.com/docs/sharing/reference/feed-dialog/v2.5
	     *
	     * @param  {String} title    Título de info
	     * @param  {String} descrip  Descripción de la info
	     * @param  {String} image    Imagen de referencia
	     * @param  {String} url      Link de la info
	     * @return {Function}        Función que devuelve el estado de la operación
	     */
	    share_dialog: function ( title, descrip, image, url ) {

	        FB.ui({
	            method:      "feed",
	            picture:     image,
	            name:        title,
	            description: descrip,
	            link:        url
	        }, function(response) {
	            if (response && !response.error_code) {

	            }
	        });
	    }
	};

	module.exports = fbDialog;

/***/ }),
/* 6 */
/***/ (function(module, exports, __webpack_require__) {

	var utility = __webpack_require__(7);

	var Home = function () {
	    this.initialize();
	}

	Home.prototype.initialize = function () {
	    this.handlers();
	}

	Home.prototype.handlers = function () {
	    var self = this;
	    self.quota();
	}

	Home.prototype.quota = function () {
	    var self = this;
	    var content = $(".count")[0];
	    var url = "../api/participants/count";

	    var promise = utility.ajax(url, 'undefined', 'undefined', 'GET');
	    promise.done(function (response) {
	        if (response.message == "count") {
	            content.innerText = response.message_descripcion;
	        } else {
	            content.innerText = "0";
	        }
	        
	    });
	}

	module.exports = Home;

/***/ }),
/* 7 */
/***/ (function(module, exports) {

	var Utility = function () {

	}

	Utility.prototype.handlers = function () {
	    var self = this;
	}

	Utility.prototype.ajax = function (url, data, headers, method) {
	    var self = this;

	    if (typeof(data) == 'undefined') {
	        data = JSON.stringify({});
	    }

	    if (typeof(headers) == 'undefined') {
	        headers = JSON.stringify({});
	    }

	    var promise = $.ajax({
	        type: method,
	        url: url,
	        dataType: 'json',
	        data: data,
	        cache: false,
	        headers: headers
	    });


	    promise.fail(function (jqXHR, textStatus, errorThrown) {
	        console.log("Error", jqXHR);
	    });

	    return promise;
	}



	module.exports = new Utility();

/***/ }),
/* 8 */
/***/ (function(module, exports) {

	var Support = function () {

	}


	Support.prototype.handlers = function () {
	    var self = this;
	    var browser = self.getBrowser();
	    console.log(browser.name, browser.version);
	    self.compatibility(browser.name, browser.version);
	    
	    
	}

	Support.prototype.getBrowser = function () {
	    var self = this;
	    var ua=navigator.userAgent,tem,M=ua.match(/(opera|chrome|safari|firefox|msie|trident(?=\/))\/?\s*(\d+)/i) || []; 
	    if (/trident/i.test(M[1])) {
	        tem=/\brv[ :]+(\d+)/g.exec(ua) || [];
	        return {name:'IE',version:(tem[1]||'')};
	    }
	    if (M[1]==='Chrome') {
	        tem=ua.match(/\bOPR|Edge\/(\d+)/)
	        if(tem!=null)   {return {name:'Opera', version:tem[1]};}
	    }
	    M=M[2]? [M[1], M[2]]: [navigator.appName, navigator.appVersion, '-?'];
	    if ((tem=ua.match(/version\/(\d+)/i))!=null) {M.splice(1,1,tem[1]);}
	    return {
	        name: M[0],
	        version: M[1]
	    };
	}

	Support.prototype.compatibility = function (name, version) {
	    var self = this;
	    var url = "/support.html";
	    switch (name) {
	        case 'Chrome':
	            if (version < 34) {
	                location.href = url;
	            }
	            break;
	        case 'Firefox':
	            if (version < 28) {
	                location.href = url;
	            }
	            break;
	        case 'IE':
	            if (version < 11) {
	                location.href = url;
	            }
	            break;
	        case 'Safari':
	            if (version < 8) {
	                location.href = url;
	            }
	            break;
	        case 'Opera':
	            if (version < 12) {
	                location.href = url;
	            }
	            break;
	    }
	}

	module.exports = new Support();

/***/ }),
/* 9 */
/***/ (function(module, exports, __webpack_require__) {

	var formValid = __webpack_require__(10);
	var utility = __webpack_require__(7);

	var Validate = function () {
	    this.initialize();
	}

	Validate.prototype.initialize = function () {
	    this.handlers();
	}

	Validate.prototype.handlers = function () {
	    var self = this;
	    jcf.replaceAll();

	    $(".cmb-document-type").on("change", function () {
	        self.filterInput($(this).val());
	    });

	    $(".btn-send-validate").on("click", function () {
	        self.validateForm();
	    });

	}

	Validate.prototype.filterInput = function (value) {
	    var self = this;
	    var document = $(".document-number");

	    document.val("");

	    if (value == "DNI") {
	        document.numeric({
	            allow: '',
	            disallow: '&,;!|"@._-·#$%&/()=¡?¿<>+*^`[]{}çœæ®†¥øπå∫∂ƒ™¶§Ω∑©√ßµ„…'
	        });
	    } else {
	        document.alphanum({
	            allow: '',
	            disallow: '&,;!|"@._-·#$%&/()=¡?¿<>+*^`[]{}çœæ®†¥øπå∫∂ƒ™¶§Ω∑©√ßµ„…'
	        });
	    }
	}


	Validate.prototype.validateForm = function () {
	    var self = this;

	    $("#frm-validate").validate(formValid);

	    if ($("#frm-validate").valid()) {
	        console.log("valido OK");
	        $("#frm-validate")[0].submit();
	    }
	}



	module.exports = Validate;

/***/ }),
/* 10 */
/***/ (function(module, exports) {

	var formValidate = {
	    debug: true,
	    errorElement: 'em',
	    errorPlacement: function (error, element) {
	        switch (element.attr("name")) {
	            case "tipDocument":
	                error.insertAfter($(".cmb-document-type").parent());
	                break;
	            case "numDocument":
	                error.insertAfter($(".document-number"));
	                break;
	        }
	    },
	    messages: {
	        tipDocument: {
	            required: 'Selecciona el tipo de documento',
	        },
	        numDocument: {
	            required: 'Ingresa el número de documento',
	            minlength: 'La cantidad de dígitos es incorrecta',
	            maxlength: 'La cantidad de dígitos es incorrecta'
	        }
	    },
	    rules: {
	        tipDocument: {
	            required: true
	        },
	        numDocument: {
	            required: true,
	            minlength: 8,
	            maxlength: 8
	        }
	    }
	};

	module.exports = formValidate;

/***/ }),
/* 11 */
/***/ (function(module, exports, __webpack_require__) {

	var formRegister = __webpack_require__(12);
	var utility = __webpack_require__(7);

	var Register = function () {
	    this.initialize();
	}

	Register.prototype.initialize = function () {
	    this.handlers();
	}

	Register.prototype.handlers = function () {
	    var self = this;
	    var dataStorage;

	    //Filtro de campos
	    self.filterInput();

	    $(".btn-send-register").on("click", function () {
	        $(".btn-send-register").prop("disabled", true);
	        self.validateForm();
	    });
	    

	    
	}

	Register.prototype.filterInput = function () {
	    var self = this;

	    $(".name, .last_name1, .last_name2, .document_number").alphanum({
	        allow: '',
	        allowSpace: true,
	        disallow: '&,;!|"@._-·`ç#$%&/()=¡?¿<>+*^`[]{}çœæ®†¥øπå∫∂ƒ™¶§Ω∑©√ßµ„…'
	    });
	    $(".email").alphanum({
	        allow: '@._-',
	        allowSpace: false,
	        disallow: ' &,;!|"·#$%&/()=¡?¿<>+*^`[]{}ñáéíóú`´çœæ®†¥øπå∫∂ƒ™¶§Ω∑©√ßµ„…'
	    });
	    $(".phone").numeric({
	        allow: '',
	        disallow: '&,;!|"@._-·#$%&/()=¡?¿<>+*^`[]{}çœæ®†¥øπå∫∂ƒ™¶§Ω∑©√ßµ„…'
	    });
	}

	Register.prototype.buildHeaders = function () {
	    var self = this;
	    return {
	        'Content-Type': 'application/x-www-form-urlencoded',
	        'Authorization': 'bearer '
	    };

	}

	Register.prototype.validateForm = function () {
	    var self = this;

	    $("#frm-register").validate(formRegister);

	    if ($("#frm-register").valid()) {
	        if ($("#g-recaptcha-response").val() != "") {
	            console.log("valido ok");
	            $(".error-captcha").hide();
	            
	            var url = "../../../api/participants";
	    
	            var data = {
	                TipoDocumento: $(".document_type").val(),
	                NumeroDocumento: $(".document_number").val(),
	                CorreoElectronico: $(".email").val(),
	                Celular: $(".phone").val(),
	                Recaptcha: $("#g-recaptcha-response").val()
	            };

	            var headers = {
	                'RequestVerificationToken': $("#token").val()
	            }
	    
	            var promise = utility.ajax(url, data, headers, 'POST');
	            promise.done(function (response) {
	                if (response.message == "success") {
	                    location.href = "/home/confirmation";
	                } else if (response.message == "E001") {
	                    //Usuario registrado
	                    location.href = "/home/exists";
	                } else if (response.message == "E002") {
	                    //Tarjeta inactiva
	                    location.href = "/home/NotActivated";
	                } else if (response.message == "E003") {
	                    //Error de recaptcha
	                    console.log("Recaptcha incorrecto");
	                    $(".error-captcha").show();
	                } else if (response.message == "E004") {
	                    //Tipo de documento incorrecto
	                    console.log("Tipo de documento incorrecto");
	                } else if (response.message == "E005") {
	                    //No hay vacante
	                    location.href = "/home/limited";
	                }
	                $(".btn-send-register").prop("disabled", false);
	            });
	        } else {
	            $(".error-captcha").css("display","block");
	            $(".btn-send-register").prop("disabled", false);
	        }
	        
	    } else {
	        $(".btn-send-register").prop("disabled", false);
	    }
	}



	module.exports = Register;

/***/ }),
/* 12 */
/***/ (function(module, exports) {

	var formRegister = {
	    debug: true,
	    errorElement: 'em',
	    /*
	    errorPlacement: function (error, element) {
	        switch (element.attr("name")) {
	            case "document_type":
	                error.insertAfter($(".cmb-document-type").parent());
	                break;
	            case "document_number":
	                error.insertAfter($(".document-number"));
	                break;
	        }
	    },
	    */
	    messages: {
	        name: {
	            required: '*Ingresa el nombre'
	        },
	        last_name1: {
	            required: '*Ingresa el apellido paterno'
	        },
	        last_name2: {
	            required: '*Ingresa el apellido materno'
	        },
	        document_number: {
	            required: '*Ingresa el número de documento'
	        },
	        email: {
	            required: '*Recuerda ingresar tu correo',
	            email: '*Ingresa un correo electrónico válido',
	            minlength: "*Ingresa un correo electrónico válido",
	            maxlength: "*El correo electrónico es muy grande"
	        },
	        phone: {
	            required: '*Recuerda ingresar tu teléfono o celular',
	            minlength: "*Ingresa un celular o teléfono válido",
	            maxlength: "*El número telefónico es muy grande"
	        }
	    },
	    rules: {
	        name: {
	            required: true
	        },
	        last_name1: {
	            required: true
	        },
	        last_name2: {
	            required: true
	        },
	        document_number: {
	            required: true
	        },
	        email: {
	            required: true,
	            correo: true,
	            email: true,
	            minlength: 6,
	            maxlength: 50
	        },
	        phone: {
	            required: true,
	            minlength: 7,
	            maxlength: 9
	        }
	    }
	};

	jQuery.validator.addMethod("correo", function(value, element) {
	    // allow any non-whitespace characters as the host part
	    return this.optional(element) || /^\w+([\.\+\-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,4})+$/.test(value);
	}, '*Ingresa un correo electrónico válido');

	module.exports = formRegister;

/***/ })
/******/ ]);