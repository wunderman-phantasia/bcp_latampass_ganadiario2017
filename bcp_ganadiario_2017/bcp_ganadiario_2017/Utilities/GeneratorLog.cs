﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web;

namespace bcp_ganadiario_2017.Utilities
{
    public class GeneratorLog
    {
        public static string GetUserIPAddress()
        {
            var context = System.Web.HttpContext.Current;
            string ip = String.Empty;

            if (context.Request.ServerVariables["HTTP_X_FORWARDED_FOR"] != null)
                ip = context.Request.ServerVariables["HTTP_X_FORWARDED_FOR"].ToString();
            else if (!String.IsNullOrWhiteSpace(context.Request.UserHostAddress))
                ip = context.Request.UserHostAddress;

            if (ip == "::1")
                ip = "127.0.0.1";

            return ip;
        }

        public static void RegisterLog(int id, string nro, string tipo_doc)
        {
            string ip = GetUserIPAddress();
            string date = DateTime.Now.ToString("dd_MM_yyyy");
            string route = ConfigurationManager.AppSettings["ROUTE_LOG"] + "/log_" + date.ToString() + ".txt";
            FileInfo perf = new FileInfo(route);

            if (!File.Exists(route))
            {
                FileStream fs = perf.Create();
                fs.Close();
            }

            perf.IsReadOnly = false;
            StreamWriter sw = new StreamWriter(perf.ToString(), true);
            string datetime = DateTime.Now.ToString(@"dd\/MM\/yyyy h\:mm:ss tt");
            switch (id)
            {
                case 1:
                    {
                        sw.WriteLine("El cliente con tipo de documento: " + tipo_doc + ", N°: " + nro + ", Intentó validar sus datos, " + "desde la ip " + ip + ", " + datetime);
                        break;
                    }
                case 2:
                    {
                        sw.WriteLine("El cliente con tipo de documento: " + tipo_doc + ", N°: " + nro + ", Ya se ha registrado anteriormente, " + "desde la ip " + ip + ", " + datetime);
                        break;
                    }
                case 3:
                    {
                        sw.WriteLine("El cliente con tipo de documento: " + tipo_doc + ", N°: " + nro + ", No pudo continuar con el registro por falta de cupo, " + "desde la ip " + ip + ", " + datetime);
                        break;
                    }
                case 4:
                    {
                        sw.WriteLine("El cliente con tipo de documento: " + tipo_doc + ", N°: " + nro + ", Se registró satisfactoriamente, " + "desde la ip " + ip + ", " + datetime);
                        break;
                    }
                case 5:
                    {
                        sw.WriteLine("El cliente con tipo de documento: " + tipo_doc + ", N°: " + nro + ", Intentó registrarse con un email duplicado, " + "desde la ip " + ip + ", " + datetime);
                        break;
                    }
                case 6:
                    {
                        sw.WriteLine("El cliente con tipo de documento: " + tipo_doc + ", N°: " + nro + ", Intentó registrarse con un celular duplicado, " + "desde la ip " + ip + ", " + datetime);
                        break;
                    }
            }
            sw.Flush();
            sw.Close();
        }

        static double ConvertBytesToMegabytes(long bytes)
        {
            return (bytes / 1024f) / 1024f;
        }
    }
}