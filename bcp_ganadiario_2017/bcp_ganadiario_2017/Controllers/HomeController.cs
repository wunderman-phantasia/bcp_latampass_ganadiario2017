﻿using bcp_ganadiario_2017.Models.BCPGanaDiario2017;
using bcp_ganadiario_2017.Models.Interface;
using bcp_ganadiario_2017.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;

namespace bcp_ganadiario_2017.Controllers
{
    public class HomeController : Controller
    {
        private BCPGANADIARIO2017Entities db = new BCPGANADIARIO2017Entities();

        // GET: Home
        public ActionResult Index()
        {
            if (Request.Browser.Type.ToUpper().Contains("IE"))
            {
                if (Request.Browser.MajorVersion <= 9)
                {

                    return RedirectToAction("Support");
                }
            }

            ViewBag.count = db.Participantes.Count();

            return View();
        }

        public ActionResult Validate()
        {
            if (Request.Browser.Type.ToUpper().Contains("IE"))
            {
                if (Request.Browser.MajorVersion <= 9)
                {

                    return RedirectToAction("Support");
                }
            }
            return View();
        }

        public ActionResult Limited()
        {
            if (Request.Browser.Type.ToUpper().Contains("IE"))
            {
                if (Request.Browser.MajorVersion <= 9)
                {

                    return RedirectToAction("Support");
                }
            }
            return View();
        }

        public ActionResult Support()
        {
            return View();
        }

        public ActionResult NotActivated()
        {
            if (Request.Browser.Type.ToUpper().Contains("IE"))
            {
                if (Request.Browser.MajorVersion <= 9)
                {

                    return RedirectToAction("Support");
                }
            }
            return View();
        }

        public ActionResult Confirmation()
        {
            if (Request.Browser.Type.ToUpper().Contains("IE"))
            {
                if (Request.Browser.MajorVersion <= 9)
                {

                    return RedirectToAction("Support");
                }
            }
            return View();
        }

        public ActionResult Exists()
        {
            if (Request.Browser.Type.ToUpper().Contains("IE"))
            {
                if (Request.Browser.MajorVersion <= 9)
                {

                    return RedirectToAction("Support");
                }
            }
            return View();
        }


        public ActionResult Register(ICliente cli)
        {
            Cliente cliente = db.Clientes.FirstOrDefault(c => c.IdCliente.Equals(cli.id));

            if (cliente == null)
            {
                return RedirectToAction("Index");
            }

            ViewBag.IdCliente = cliente.IdCliente;
            ViewBag.DesTipDoc = RijndaelSimple.Decrypt(cliente.DesTipDoc);
            ViewBag.NumDoc = RijndaelSimple.Decrypt(cliente.NumDoc);
            ViewBag.NbrCLI = RijndaelSimple.Decrypt(cliente.NbrCLI);
            ViewBag.ApePatCli = RijndaelSimple.Decrypt(cliente.ApePatCli);
            ViewBag.ApeMatCli = RijndaelSimple.Decrypt(cliente.ApeMatCli);
            ViewBag.Meta = RijndaelSimple.Decrypt(cliente.Meta);
            ViewBag.Bono = RijndaelSimple.Decrypt(cliente.Bono);

            return View();
        }

        void ValidateRequestHeader(HttpRequestMessage request)
        {
            string cookieToken = "";
            string formToken = "";

            IEnumerable<string> tokenHeaders;
            if (request.Headers.TryGetValues("RequestVerificationToken", out tokenHeaders))
            {
                string[] tokens = tokenHeaders.First().Split(':');
                if (tokens.Length == 2)
                {
                    cookieToken = tokens[0].Trim();
                    formToken = tokens[1].Trim();
                }
            }
            AntiForgery.Validate(cookieToken, formToken);
        }

    }
}