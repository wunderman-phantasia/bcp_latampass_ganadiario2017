﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using bcp_ganadiario_2017.Models.BCPGanaDiario2017;
using bcp_ganadiario_2017.Utilities;

namespace bcp_ganadiario_2017.Controllers
{
    public class Participantes_QAController : ApiController
    {
        private BCPGANADIARIO2017Entities db = new BCPGANADIARIO2017Entities();



        // POST: api/Participantes_QA
        [HttpGet]
        [ResponseType(typeof(Participantes_QA))]
        public IHttpActionResult PostParticipantes_QA(string Nombres, string ApellidoPaterno, string ApellidoMaterno, string TipoDocumento, string NumeroDocumento, string CorreoElectronico, string Celular)
        {
            Participantes_QA participantes_QA = new Participantes_QA();
            participantes_QA.IdParticipante = Guid.NewGuid();
            participantes_QA.Nombres = RijndaelSimple.Encrypt(Nombres);
            participantes_QA.ApellidoPaterno = RijndaelSimple.Encrypt(ApellidoPaterno);
            participantes_QA.ApellidoMaterno = RijndaelSimple.Encrypt(ApellidoMaterno);
            participantes_QA.TipoDocumento = RijndaelSimple.Encrypt(TipoDocumento);
            participantes_QA.NumeroDocumento = RijndaelSimple.Encrypt(NumeroDocumento);
            participantes_QA.CorreoElectronico = RijndaelSimple.Encrypt(CorreoElectronico);
            participantes_QA.Celular = RijndaelSimple.Encrypt(Celular);


            db.Participantes_QA.Add(participantes_QA);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateException)
            {
                if (Participantes_QAExists(participantes_QA.IdParticipante))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = participantes_QA.IdParticipante }, participantes_QA);
        }


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool Participantes_QAExists(Guid id)
        {
            return db.Participantes_QA.Count(e => e.IdParticipante == id) > 0;
        }
    }
}