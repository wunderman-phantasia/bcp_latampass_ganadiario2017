﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace bcp_ganadiario_2017
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: "Register",
                url: "home/register",
                defaults: new { controller = "Home", action = "Register", id = UrlParameter.Optional }
            );

            routes.MapRoute(
               name: "RegisterCustomer",
               url: "home/register/customer/{id}",
               defaults: new { controller = "Home", action = "Register", id = UrlParameter.Optional }
           );


            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );

            routes.MapRoute(
               name: "Limited",
               url: "home/limited",
               defaults: new { controller = "Home", action = "Limited"}
           );

            routes.MapRoute(
              name: "NotActivate",
              url: "home/notactivated",
              defaults: new { controller = "Home", action = "NotActivated"}
          );

            routes.MapRoute(
             name: "Exists",
             url: "home/exists",
             defaults: new { controller = "Home", action = "Exists" }
         );

            routes.MapRoute(
           name: "Confirmation",
           url: "home/confirmation",
           defaults: new { controller = "Home", action = "Confirmation"}
       );

        }
    }
}
