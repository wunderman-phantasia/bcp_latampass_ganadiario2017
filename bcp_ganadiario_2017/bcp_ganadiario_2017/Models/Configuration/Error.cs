﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace bcp_ganadiario_2017.Models.Configuration
{
    public class Error
    {
        public string error { get; set; }
        public string error_descripcion { get; set; }
    }
}