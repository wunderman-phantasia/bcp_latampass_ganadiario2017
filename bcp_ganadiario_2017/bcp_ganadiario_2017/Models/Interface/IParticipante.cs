﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace bcp_ganadiario_2017.Models.Interface
{
    public class IParticipante
    {
        public System.Guid IdParticipante { get; set; }
        public string TipoDocumento { get; set; }
        [Required, MaxLength(8)]
        public string NumeroDocumento { get; set; }
        [Required, EmailAddress, MaxLength(50)]
        public string CorreoElectronico { get; set; }
        [Required, MaxLength(9), RegularExpression("^[0-9]*$", ErrorMessage = "Celular must be numeric")]
        public string Celular { get; set; }
        //[Required]
        public string Recaptcha { get; set; }
    }
}